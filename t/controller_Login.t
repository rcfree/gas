use strict;
use warnings;
use Test::More;

BEGIN { use_ok 'Catalyst::Test', 'GAS' }
BEGIN { use_ok 'GAS::Controller::Login' }

ok( request('/login')->is_success, 'Request should succeed' );
done_testing();
