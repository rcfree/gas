#!perl -T

use Test::More tests => 1;

BEGIN {
    use_ok( 'GAS' );
}

diag( "Testing GAS $GAS::VERSION, Perl $], $^X" );
