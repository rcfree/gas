use strict;
use warnings;
use Test::More tests => 3;

BEGIN { use_ok 'Catalyst::Test', 'GAS' }
BEGIN { use_ok 'GAS::Controller::AccessLevel' }

ok( request('/accesslevel')->is_success, 'Request should succeed' );


