package GAS::Schema;

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_classes;


# Created by DBIx::Class::Schema::Loader v0.04006 @ 2010-01-20 15:01:14
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:FYGQH+ruGTlc2/1tQpk9Fg


# You can replace this text with custom content, and it will be preserved on regeneration
1;
