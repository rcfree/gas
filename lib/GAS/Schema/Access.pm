package GAS::Schema::Access;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("Core");
__PACKAGE__->table("access");
__PACKAGE__->add_columns(
  "access_id",
  { data_type => "INT", default_value => undef, is_nullable => 0, size => 11 },
  "user_id",
  { data_type => "INT", default_value => undef, is_nullable => 0, size => 11 },
  "access_level",
  {
    data_type => "VARCHAR",
    default_value => undef,
    is_nullable => 1,
    size => 10,
  },
  "resource_id",
  { data_type => "INT", default_value => undef, is_nullable => 0, size => 11 },
  "date",
  {
    data_type => "DATETIME",
    default_value => undef,
    is_nullable => 1,
    size => 19,
  },
  "response",
  {
    data_type => "VARCHAR",
    default_value => undef,
    is_nullable => 1,
    size => 10,
  },
);
__PACKAGE__->set_primary_key("access_id");
__PACKAGE__->add_unique_constraint("single_access", ["user_id", "resource_id"]);
__PACKAGE__->belongs_to("user_id", "GAS::Schema::User", { user_id => "user_id" });
__PACKAGE__->belongs_to(
  "resource_id",
  "GAS::Schema::Resource",
  { resource_id => "resource_id" },
);


# Created by DBIx::Class::Schema::Loader v0.04006 @ 2010-01-20 15:01:14
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:JxVheym4oZu7k032WHfzgw


# You can replace this text with custom content, and it will be preserved on regeneration
1;
