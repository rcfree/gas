package GAS::Controller::Resource;

use strict;
use warnings;
use parent 'Catalyst::Controller::REST';
use Data::Dumper qw(Dumper);

=head1 NAME

GAS::Controller::Resource - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub resource :Path('') :ActionClass('REST') { }

sub resource_PUT {
	my ($self, $c) = @_;

	my $curr_user = $c->get_user();
	
	my $uri = $c->req->param('uri');
	
	if (!$curr_user->can_create_resource) {
		$self->status_bad_request($c, message => "Logged in user (".$curr_user->identity .") does not have permission to create resource '$uri'");
		return;
	}
	
	my $anon_level = $c->req->param('anon_level');
	my $user_level = $c->req->param('user_level');
	
	my $rsrc;
	eval {
		$rsrc = $c->model('DB')->resultset('Resource')->create({
			'uri'=>$uri,
		});
		my $anon = $c->model('DB')->resultset('User')->search({
			identity => 'anon'
		})->single;
		my $user = $c->model('DB')->resultset('User')->search({
			identity => 'user'
		})->single;
		
		$rsrc->create_related('accesses',{ user_id => $curr_user->id, access_level => 'admin' });
		$rsrc->create_related('accesses',{ user_id => $anon->id, access_level => $anon_level });
		if ($user->id != $curr_user->id ) {
			$rsrc->create_related('accesses',{ user_id => $user->id, access_level => $user_level });
		}
	};
	
	if ($@) {
		$self->status_bad_request($c, message => 'Unable to create resource '. $@);
		return;
	}
	my %rsrc_data = $rsrc->get_columns();
	$self->status_created(
		$c,
		location => $c->req->uri->as_string,
		entity => \%rsrc_data
	);
}

sub resource_GET {
	my ($self, $c) = @_;
	my $curr_user = $c->get_user();
	my $uri = $c->req->param('uri');
	
	my $dc_rsrc = $c->model('DB')->resultset('Resource')->search({
			'uri'=>$uri,
	})->single;
	
	if (!$dc_rsrc) {
		$self->status_bad_request($c, message => 'Resource does not exist. Unable to retrieve.');
		return;
	}
	
	my @dc_accesses = $dc_rsrc->accesses;
	
	my $r = {
		resource=>{$dc_rsrc->get_columns()}
	};
	
	my @accesses = ();
	foreach my $a(@dc_accesses) {
		my $access = {$a->get_columns()};
		$access->{user}={$a->user_id->get_columns()};
		push @accesses, $access;
	}
	$r->{accesses}=\@accesses;
	
	$self->status_ok($c, entity => $r);
}

sub resource_DELETE {
	my ($self, $c) = @_;
	my $curr_user = $c->get_user();
	
	if (!$curr_user->can_delete_resource) {
		$self->status_bad_request($c, message => 'Logged in user does not have permission to delete resource');
		return;
	}
	my $uri = $c->req->param('uri');
	my $rsrc = $c->model('DB')->resultset('Resource')->search({'uri'=>$uri})->delete;
	
	if ($rsrc) {
		$self->status_ok($c, entity => {});
	}
	else {
		$self->status_bad_request($c, message => 'Resource does not exist. Unable to delete.');
		return;
	}
}

=head1 AUTHOR

Rob Free

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;