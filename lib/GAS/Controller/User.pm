package GAS::Controller::User;

use strict;
use warnings;
use parent 'Catalyst::Controller::REST';
use Data::Dumper qw(Dumper);

=head1 NAME

GAS::Controller::User - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub user :Path('') :ActionClass('REST') { }

sub user_PUT {
	my ($self, $c) = @_;
	my $curr_user = $c->get_user();
	if (!$curr_user->can_create_user) {
		$self->status_bad_request($c, message => 'Logged in user does not have permission to create user');
		return;
	}
	
	my $identity = $c->req->param('identity');
	my $name = $c->req->param('name');
	
	my $user;
	eval {
		$user = $c->model('DB')->resultset('User')->create({
			'identity'=>$identity,
			'nickname'=>$name
		});
	};
	
	if ($@) {
		$self->status_bad_request($c, message => "Unable to create user ($identity) ". $@);
		return;
	}
	my %user_data = $user->get_columns();
	$c->log->info("user_data:".Dumper(\%user_data));
	$self->status_created(
		$c,
		location => $c->req->uri->as_string,
		entity => \%user_data
	);
}

sub user_DELETE {
	my ($self, $c) = @_;
	my $curr_user = $c->get_user();
	if (!$curr_user->can_delete_user) {
		$self->status_bad_request($c, message => 'Logged in user does not have permission to delete user');
		return;
	}
	my $identity = $c->req->param('identity');
	my $user = $c->model('DB')->resultset('User')->search({'identity'=>$identity})->delete;
	
	if ($user) {
		$self->status_ok($c, entity => {});
	}
	else {
		$self->status_bad_request($c, message => 'User does not exist. Unable to delete.');
		return;
	}
}

sub user_POST {
	my ($self, $c) = @_;
	my $curr_user = $c->get_user();
	if (!$curr_user->can_update_user) {
		$self->status_bad_request($c, message => 'Logged in user does not have permission to update user');
		return;
	}
	my $identity = $c->req->param('identity');
	my $name = $c->req->param('nickname');
	$c->log->info("identity:$identity");
	my $user = $c->model('DB')->resultset('User')->search({'identity'=>$identity})->single;
	if (!$user) {
		$self->status_bad_request($c, message => 'User does not exist. Unable to update.');
		return;
	}
	
	my $success = $user->update({
		nickname=>$name
	});
	my %user_data = $user->get_columns();
	$self->status_ok($c, entity => \%user_data);
}

sub user_GET {
	my ($self, $c) = @_;
	my $identity = $c->req->param('identity');
	my $id = $c->req->param('user_id');
	my $all = $c->req->param('all');
	
	if ($all) {
		my @users = $c->model('DB')->resultset('User')->search->all;
	
		$c->log->info("Returning all users");
		my @user_data = map { {$_->get_columns()} } @users;
		$self->status_ok($c, entity => {user => \@user_data} );
	}
	else {
		my $search;
		if ($identity) {
			$search = {'md5'=>$identity};
		}
		if ($id) {
			$search = {'user_id'=>$id};
		}
		
		if (!$search) {
			$self->status_bad_request($c, message => "Must specify 'identity' or 'user_id' to retrieve user");
			return;
		}
		
		my $user = $c->model('DB')->resultset('User')->search($search)->single;
	
		if (!$user) {
			$self->status_bad_request($c, message => "User $identity does not exist. Unable to retrieve.");
			return;
		}
		$c->log->info("Returning user $identity");
		my %user_data = $user->get_columns();
		$self->status_ok($c, entity => \%user_data);
	}
}

=head1 AUTHOR

Rob Free

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
