package GAS::Controller::Login;

use parent 'Catalyst::Controller::REST';

=head1 NAME

GAS::Controller::Login - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub login :Path('') :ActionClass('REST') { }

sub login_POST {
	my ($self, $c) = @_;
	my $identity = $c->req->param('identity');
	
	if (!$identity) {
		$c->log->info("failed");
		$self->status_bad_request(
			$c,
			message => "Login failed. User not specified."
		);
		return;
	}
	
	my $user = $c->model('DB')->resultset('User')->search({'md5'=>$identity})->single;
	if (!$user) {
		$c->log->info("failed");
		$self->status_bad_request(
			$c,
			message => "Login failed. User $identity does not exist"
		);
		return;
	}
	#my %user_data = $user->get_columns();
	#$c->session->{user}=\%user_data;
	#$c->log->info("session:".Dumper($c->session));
	$c->log->info("success");
	$self->status_ok(
		$c,
		entity => {'status'=>'success'}
	);
}

=head1 AUTHOR

root

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut


1;
