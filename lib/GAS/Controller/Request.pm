package GAS::Controller::Request;

use strict;
use warnings;
use parent 'Catalyst::Controller::REST';
use Data::Dumper qw(Dumper);

=head1 NAME

GAS::Controller::Request - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub request :Path('') :ActionClass('REST') { }

sub request_GET {
	my ($self, $c) = @_;
	my $uri = $c->req->param('uri');
	
	my $curr_user = $c->get_user();
	
	my $rsrc_pars = { 'user_id.user_id'=>$curr_user->id, 'access_level'=>'admin'};
	$uri and $rsrc_pars->{uri}=$uri;
	
	my @rsrc_admin = $c->model('DB')->resultset('Resource')->search($rsrc_pars, {join=>{'accesses'=>'user_id'}})->all;
	
	my @rsrc_ids = map { $_->id } @rsrc_admin;

	my @requests = ();
	foreach my $req($c->model('DB')->resultset('Access')->search({ resource_id=>{'in'=>\@rsrc_ids}, access_level => 'pending'})->all) {
		push @requests, {
			user => { $req->user_id->get_columns() },
			resource => $req->resource_id->uri
		};
	}

	$self->status_ok(
		$c,
		entity => {'requests'=>\@requests}
	);
}

sub request_PUT {
	my ($self, $c) = @_;
	my $uri = $c->req->param('uri');
	
	my $curr_user = $c->get_user();
	
	my $rsrc = $self->get_data($c,'Resource',{'uri'=>$uri}) or return;
	
	my $request = $rsrc->search_related('accesses',{'user_id'=>$curr_user->id})->single;
	
	if ($request) {
		if ($request->access_level eq 'full' || $request->access_level eq 'limited' || $request->access_level eq 'admin') {
			$self->status_bad_request($c, message => 'Access already granted to this resource');
			return;
		}
		elsif ($request->access_level eq 'pending') {
			$self->status_bad_request($c, message => 'Access to this resource is already pending');
			return;
		}
		else {
			$request->update({ access_level => 'pending' } );
		}
	}
	else {
		$request = $rsrc->accesses->create({'user_id'=>$curr_user->id,'access_level'=>'pending'});
	}
	
	my %req_data = $request->get_columns();
	$c->log->info("req_data:".Dumper(\%req_data));
	$self->status_created(
		$c,
		location => $c->req->uri->as_string,
		entity => \%req_data
	);
}

sub get_data {
	my ($self, $c, $table, $pars ) = @_;
	my $row = $c->model('DB')->resultset($table)->search($pars)->single;
	$c->log->info("pars:".Dumper($pars));
	if (!$row) {
		$self->status_bad_request($c, message => "$table does not exist. Unable to retrieve.");
		return;
	}
	return $row;
}
#
#sub get_user {
#	my ($self, $c) = @_;
#	my $user = $c->session->{user};
#	$c->log->info("user:".Dumper($user));
##	if (!$user) {
##		$self->status_bad_request($c, message => 'Not logged in. Unable to access GAS.');
##		return 0;
##	}
#	return $user;
#}

=head1 AUTHOR

Rob Free

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
