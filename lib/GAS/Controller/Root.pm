package GAS::Controller::Root;

use strict;
use warnings;
use parent 'Catalyst::Controller::REST';
use Data::Dumper qw(Dumper);
use XML::Simple;

#
# Sets the actions in this controller to be registered with no prefix
# so they function identically to actions created in MyApp.pm
#
__PACKAGE__->config->{namespace} = '';

=head1 NAME

GAS::Controller::Root - Root Controller for GAS

=head1 DESCRIPTION

[enter your description here]

=head1 METHODS

=cut

=head2 index

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    # Hello World
    $c->response->body( $c->welcome_message );
}

sub begin {
	my ($self, $c) = @_;
	#$c->log->info("session:".Dumper($c->session));
}
sub default :Path {
    my ( $self, $c ) = @_;
    $c->response->body( 'Page not found' );
    $c->response->status(404);
}

##get level of access for resource
#sub accesslevel :Path('accesslevel') {
#	my ($self, $c) = @_;
#	my $id = $c->req->param('id');
#	my $uri = $c->req->param('uri');
#	$c->response->body("$id $uri");
#}

#add/delete user
#sub user :Path('user') {
#	my ($self, $c) = @_;
#	my $identity = $c->req->param('identity');
#	my $name = $c->req->param('name');
#	$c->model('DB')->resultset('User')->create({
#		'identity'=>$identity,
#		'name'=>$name
#	});
#}

#add/delete resource
#sub resource :Path('resource') {
#	my ($self, $c) = @_;
#	my $uri = $c->req->param('uri');
#	
#	$c->model('DB')->resultset('Resource')->create({
#		'uri'=>$uri,
#	});
#}

#add/delete resource
#sub request :Path('request') {
#	my ($self, $c) = @_;
#	my $uri = $c->req->param('uri');
#	my $identity = $c->req->param('identity');
#	
#	my $resource = $c->model('DB')->resultset('Resource')->search({uri=>$uri});
#	!$resource and die "No resource with this URI";
#	
#	$c->model('DB')->resultset('Access')->create({
#		'uri'=>$uri,
#	});
#}


#sub login :Path('login') :ActionClass('REST') { }
#


=head2 end

Attempt to render a view, if needed.

=cut

#sub end : ActionClass('RenderView') {}

=head1 AUTHOR

Rob Free

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
