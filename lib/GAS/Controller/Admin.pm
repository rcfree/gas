package GAS::Controller::Admin;

use strict;
use warnings;
use parent 'Catalyst::Controller::REST';
use Data::Dumper qw(Dumper);

=head1 NAME

GAS::Controller::Admin - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub admin :Path('') :ActionClass('REST') { }

sub admin_GET {
	my ($self, $c) = @_;

	my $curr_user = $c->get_user();
	my $count = $c->req->param('count');
	my $page = $c->req->param('page');
	my $page_size = $c->req->param('page_size');
	my $query = $c->req->param('query');
	my @uri = $c->req->param('uri');
	
#	if (!$user) {
#		$self->status_bad_request($c, message => 'Not logged in. Unable to get access information.');
#		return;
#	}
	
#	my $resource = $c->model('DB')->resultset('Resource')->search({'uri'=>$uri})->single;
#	
#	if (!$resource) {
#		$self->status_bad_request($c, message => "Resource URI '$uri' does not exist. Unable to get access information.");
#		return;
#	}
	
	my $anon = $c->model('DB')->resultset('User')->search({
			identity => 'anon'
	})->single;
	
	my $user = $c->model('DB')->resultset('User')->search({
			identity => 'user'
	})->single;
	
	my $pars = {'access_level'=>'admin'};
	
	if ($query) {
		$pars->{'resource_id.uri'}={'like'=>'%'.$query.'%'};
	}
	if (length($uri[0])>0) {
		$pars->{'resource_id.uri'}={'in'=>\@uri};
	}
	
	my $opts = {join=>'resource_id'};
	if (!$count && !@uri) {
		$opts->{page}=$page;
		$opts->{rows}=$page_size;
	} 
	$opts->{order_by}='resource_id.uri';
	
	my $access_rs = $curr_user->accesses->search($pars, $opts);
	
	if ($count) {
		$self->status_ok($c, entity => { 'count' => $access_rs->count });
	}
	else {
		my @resources = ();
		foreach my $acc($access_rs->all) {
			my $rsrc = $acc->resource_id;
			
			#get anon and user levels for resource
			my $anon_acc = $rsrc->search_related('accesses',{'user_id'=>$anon->id})->single;
			my $user_acc = $rsrc->search_related('accesses',{'user_id'=>$user->id})->single;
			my $anon_level = $anon_acc ? $anon_acc->access_level : 'nogas';
			my $user_level = $user_acc ? $user_acc->access_level : 'nogas';
			
			push @resources, { 
				uri=>$rsrc->uri, 
				anon_level=>$anon_level,
				user_level=>$user_level,
			};
		}
		$self->status_ok($c, entity => { resource => \@resources} );
	}
}

=head1 AUTHOR

Rob Free

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
