package GAS::Controller::Access;

use strict;
use warnings;
use parent 'Catalyst::Controller::REST';
use Data::Dumper qw(Dumper);

=head1 NAME

GAS::Controller::AccessLevel - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub access :Path('') :ActionClass('REST') { }

sub access_GET {
	my ($self, $c) = @_;
	my $identity = $c->req->param('identity');
	my $uri = $c->req->param('uri');
	my $curr_user = $c->get_user();
	
#	if (!$user) {
#		$self->status_bad_request($c, message => 'Not logged in. Unable to get access information.');
#		return;
#	}
	
	my $resource = $c->model('DB')->resultset('Resource')->search({'uri'=>$uri})->single;
	
	if (!$resource) {
		$self->status_bad_request($c, message => "Resource URI '$uri' does not exist. Unable to get access information.");
		return;
	}
	
	my $access = $c->model('DB')->resultset('Access')->search({'user_id'=>$curr_user->id, 'resource_id'=>$resource->id})->single;
	my $level;
	if (!$access) {
		if ($curr_user->identity eq 'anon') {
			$self->status_ok($c, entity => { access_level => 'none' });
			return;
		}
		else {
			$access = $c->model('DB')->resultset('Access')->search({'user_id.identity'=>'user', 'resource_id'=>$resource->id}, {join => 'user_id' })->single;
		}
	}
	my %acc_data = $access->get_columns();
	$c->log->info("acc_data:".Dumper(\%acc_data));
	$self->status_ok($c, entity => \%acc_data);
}

sub access_PUT {
	my ($self, $c) = @_;
	my $curr_user = $c->get_user();
	my $user_id = $c->req->param('user_id');
	my $uri = $c->req->param('uri');
	my $level = $c->req->param('level');
	my $response = $c->req->param('response');
	my $identity = $c->req->param('identity');
	$c->log->info("params:".Dumper($c->req->params));
	my $rsrc = $c->model('DB')->resultset('Resource')->search({'uri'=>$uri})->single;
	
	if (!$rsrc) {
		$self->status_bad_request($c, message => "Resource URI '$uri' does not exist");
		return;
	}
	$c->log->info("md5 identity:".$identity);
	my $user_params = $identity ? {'md5'=>$identity} : {'user_id'=>$user_id};
	
	my $user = $c->model('DB')->resultset('User')->search($user_params)->single;
	
	if (!$user) {
		$self->status_bad_request($c, message => "User with ID/identity ".($user_id || $identity)." does not exist");
		return;
	}
	
	my $is_admin = $rsrc->search_related('accesses',{ access_level => 'admin', user_id => $curr_user->user_id })->count;
	
	if ($is_admin==0) {
		$self->status_bad_request($c, message => "Logged in user (". $curr_user->identity.") does not have permission to set access permissions for resource '$uri'");
		return;
	}
	
	my $acc;
	eval {
		$acc = $rsrc->find_related('accesses',{ user_id => $user->id });
		if ($acc) {
			$acc->update({access_level => $level, response => $response });
		}
		else {
			$acc = $rsrc->create_related('accesses',{ user_id => $user->id, access_level => $level, response => $response });
		}
	};
	if ($@) {
		$self->status_bad_request($c, message => 'Unable to create access permission '. $@);
		return;
	}
	my %acc_data = $rsrc->get_columns();
	$self->status_created(
		$c,
		location => $c->req->uri->as_string,
		entity => \%acc_data
	);
}

sub access_DELETE {
	my ($self, $c) = @_;
	my $identity = $c->req->param('identity');
	my $user_id = $c->req->param('user_id');
	my $uri = $c->req->param('uri');
	my $curr_user = $c->get_user();

	my $resource = $c->model('DB')->resultset('Resource')->search({'uri'=>$uri})->single;
	
	if (!$resource) {
		$self->status_bad_request($c, message => "Resource URI '$uri' does not exist. Unable to delete access information.");
		return;
	}
	
	my $user_params = $identity ? {'md5'=>$identity} : {'user_id'=>$user_id};
	
	my $user = $c->model('DB')->resultset('User')->search($user_params)->single;
	
	if (!$user) {
		$self->status_bad_request($c, message => "User does not exist. Unable to delete access information.");
		return;
	}
	
	my $access = $c->model('DB')->resultset('Access')->search({'user_id'=>$user->id, 'resource_id'=>$resource->id})->single;
	
	if (!$access) {
		$self->status_bad_request($c, message => "Access for URI '$uri' and user does not exist. Unable to delete access information.");
		return;
	}
	$access->delete;
	$self->status_ok($c, entity => {});
}

=head1 AUTHOR

Rob Free

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
