package GAS::Model::DB;

use strict;
use base 'Catalyst::Model::DBIC::Schema';

__PACKAGE__->config(
    schema_class => 'GAS::Schema',
    connect_info => [
        'dbi:mysql:GAS',
        'root',
        'g3n0m3',
        
    ],
);



=head1 NAME

GAS::Model::DB - Catalyst DBIC Schema Model
=head1 SYNOPSIS

See L<GAS>

=head1 DESCRIPTION

L<Catalyst::Model::DBIC::Schema> Model using schema L<GAS::Schema>

=head1 AUTHOR

Rob Free

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
