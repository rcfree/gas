package GAS;

use strict;
use warnings;

use Catalyst::Runtime 5.80;
use Env;
# Set flags and add plugins for the application
#
#         -Debug: activates the debug mode for very useful log messages
#   ConfigLoader: will load the configuration from a Config::General file in the
#                 application's home directory
# Static::Simple: will serve static files from the application's root
#                 directory

use parent qw/Catalyst/;
use Catalyst qw/-Debug
                ConfigLoader
                Static::Simple
                /;
our $VERSION = '0.01';
$ENV{DBIC_TRACE}         = 1;

# Configure the application.
#
# Note that settings in gas.conf (or other external
# configuration file that you set up manually) take precedence
# over this when using ConfigLoader. Thus configuration
# details given here can function as a default configuration,
# with an external configuration file acting as an override for
# local deployment.

__PACKAGE__->config( name => 'GAS' );

# Start the application
__PACKAGE__->setup();


sub get_user {
	my ($c)=@_;
	my $id = $c->req->param('user');
	$c->log->info("user:$id");
	my $user = $c->model('DB')->resultset('User')->search({ 'md5' => $id })->single;
	$user ? $c->log->info("db_user:".$user->get_columns()) : $c->log->info("no user");
	return $user;
}

=head1 NAME

GAS - Catalyst based application

=head1 SYNOPSIS

    script/gas_server.pl

=head1 DESCRIPTION

[enter your description here]

=head1 SEE ALSO

L<GAS::Controller::Root>, L<Catalyst>

=head1 AUTHOR

Rob Free

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
